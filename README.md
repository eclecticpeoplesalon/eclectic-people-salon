

Eclectic People Salon is conveniently located across from Texas Tech University in Lubbock, TX. We use L'ANZA Healing Haircare and specialize in creative hair color, Balayage and color melting, highlights and extensions, waxing, haircuts, replacements and more. Get your best hair. Book today!

Address: 2422 Broadway St, Suite A, Lubbock, TX 79401, USA

Phone: 806-771-5264

